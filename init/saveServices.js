"use strict";

var AWS = require('aws-sdk');
var fs = require('fs');
var CONSTANTS = require('../lib/constants.js');
var AWS_CONFIG = require('../config/awsConfig.js');

AWS.config.update({
  region : AWS_CONFIG.AWS_CONFIG_REASON
});

var docClient = new AWS.DynamoDB.DocumentClient();

function saveData(){
  var obj;
  fs.readFile('../config/serviceData.json', 'utf8', function (err, serviceData) {
    if (err) throw err;
    obj = JSON.parse(serviceData);
    obj.forEach(function(service){  
      var params = {
        TableName : CONSTANTS.TAPCLICKS_FLASHBRIEFING_TABLE,
        Item: service
      };
      saveService(params).then(function(data){
        console.log(data);
      },function(error){
        console.log(error);
      });
    });
  });
}

function saveService(params){
  return new Promise(function(resolve, reject){
    docClient.put(params, function(err, data) {
      if (err) {
        reject(err);
      } 
      else {
        resolve(data);
      }
    });
  });  
}

saveData();

