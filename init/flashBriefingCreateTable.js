"use strict";

var AWS = require("aws-sdk");
var CONSTANTS = require('../lib/constants.js');
var AWS_CONFIG = require('../config/awsConfig.js');

AWS.config.update({
    region : AWS_CONFIG.AWS_CONFIG_REASON
});


var dynamodb = new AWS.DynamoDB({httpOptions: {timeout: 5000}});
var dynamodb = new AWS.DynamoDB();

function createFlashBriefingTable(){
    var params = {
        TableName : CONSTANTS.TAPCLICKS_FLASHBRIEFING_TABLE,
        KeySchema: [       
            { AttributeName: "user", KeyType: "HASH" } 
        ],
        AttributeDefinitions: [       
            { AttributeName: "user", AttributeType: "S" }
        ],
        ProvisionedThroughput: {       
            ReadCapacityUnits: 10, 
            WriteCapacityUnits: 10
        }
    };
    return new Promise(function(resolve, reject){
        dynamodb.createTable(params, function(err, data) {
            if (err) {
                reject("Unable to create table. Error JSON : \n" +  JSON.stringify(err, null, 2));
            } else {
                resolve("Created table. Table description JSON : \n"  + JSON.stringify(data, null, 2));
            }
         });
    });
}

createFlashBriefingTable().then(function(response){
    console.log(response);
},function(error){
    console.log(error);
});


