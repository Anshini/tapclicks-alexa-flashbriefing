"use strict";

var AmazonDateParser = require('amazon-date-parser');
const unirest = require('unirest');
var CONSTANTS = require('../lib/constants.js');
var DAO = require('../dao/userDao.js');

function fetchUserData(url, service, metric){
	return new Promise(function(resolve, reject){
		DAO.getAccessToken().then(function(token){
			var access_token = token.Item.info;
			unirest.get(url)
				.headers({ "Authorization": "Bearer " + access_token, "accept": "application/json" })
				.send()
				.end(function(response){
					if (response.statusCode == 200 && response.body && response.body.data) {
						var data = response.body.data;
						var totalData = 0;
						data.forEach(function(metricsData){
							if(metricsData[metric.field] != null){
								var value =  parseInt(metricsData[metric.field]);
								totalData = totalData + value;
							}
						});
						var result ={
							service : service,
							lable : metric.lable,
							value : totalData
						}
						resolve(result);
					}
					else{
						reject("no data present");
					}
				});
			}, function(error){
 		 	reject(error);
			});
	});
};

function createUrl(serviceId, metric){
	var d = new Date();
    d.setDate(d.getDate() - 1);
    var date = d.getFullYear() + "-" + (d.getMonth() + 1 ) + "-" + d.getDate();
    var dateRange = date + "|" + date;
    var url = CONSTANTS.API_BASE_URL + serviceId + "/data/" + metric.associated_dataview_id + "?daterange=" + dateRange + "&fields=" + metric.field + "&timegrouping=hourly";
    return url;
};


module.exports = {
	searchUserData : function(user){
	return new Promise(function(resolve, reject){
		var serviceArray =[];
		var serviceJson = [];
			DAO.getUser(user).then(function(data){
				data = data.Item.info;
				data.forEach(function(service){
					serviceJson.push(service.service);
					var metric = service.metric;
					metric.forEach(function(metricData){
						var url = createUrl(service.service_id, metricData);
						serviceArray.push(fetchUserData(url, service.service, metricData));
					});
				});
				Promise.all(serviceArray).then(values => { 
					var speechOut = " Yesterday " ;
					serviceJson.forEach(function(searchService){
						var i =1;
						var res = " for " + searchService + ", you had , ";
						values.forEach(function(val){
							if(searchService == val.service){
								res = res + val.value + " " + val.lable + " , "
							}
						});
						speechOut = speechOut + res;
					});
					resolve(speechOut);
					}, reason => {
					  console.log(reason)
					});
			}, function(error){
					reject(error);
			});
		});
	}
};

