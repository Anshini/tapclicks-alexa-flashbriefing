"use strict";

var AWS = require('aws-sdk');
var CONSTANTS = require('../lib/constants.js');
var AWS_CONFIG = require('../config/awsConfig.js');

AWS.config.update({
  region : AWS_CONFIG.AWS_CONFIG_REASON
});

var docClient = new AWS.DynamoDB.DocumentClient();

module.exports ={
	getUser : function(user){
		var params = {
			TableName : CONSTANTS.TAPCLICKS_FLASHBRIEFING_TABLE,
			Key : {
				"user" : user
			}
		};
		return new Promise(function(resolve, reject){
			docClient.get(params, function(err, data) {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		});
	},
	getAccessToken : function(){
		var params = {
		  TableName : CONSTANTS.OAUTH_TABLE,
		  Key : {
		    "access_token" : "access_token"
		  }
		};
		return new Promise(function(resolve, reject){
		  docClient.get(params, function(err, data) {
		    if (err) {
		      reject(err);
		    } 
		    else {
		      resolve(data);
		    }
		  });
		});  
  	}
}