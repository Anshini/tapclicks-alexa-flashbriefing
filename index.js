'use strict';

var uniqid = require('uniqid');
var SERVICE = require('./service/userService.js');
 

exports.handler = function(event, context, callback) {
    var uid = uniqid();
    var date = new Date();
   
    SERVICE.searchUserData("default").then(function(text){
         var responseBody =  {
            "uid": "urn:uuid:" + uid,
            "updateDate": date,
            "titleText": "Alexa Flash Briefing Developer",
            "mainText":  text
        };
        console.log("response: " + JSON.stringify(responseBody))
        callback(null, responseBody);         
    },function(error){
        console.log(error);
    });
};